@include('base.header')
 

<div class="site-section bg-info">
    <div class="container">
      <div class="row mb-5">
        <div class="col-12 text-center">
          <span class="text-cursive h5 text-red d-block">Packages You Like</span>
          <h2 class="text-white">Our Packages</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 mb-4 mb-lg-0">
          <div class="package text-center bg-white">
            <span class="img-wrap"><img src="{{url('assets/images/flaticon/svg/001-jigsaw.svg')}}" alt="Image" class="img-fluid"></span>
            <h3 class="text-teal">Title 1</h3>
            <div class="card-body card-block">
                <div class="form-group">
                    <input type="text" class="form-control" name="first_name" placeholder="Enter Length...">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="last_name" placeholder="Enter Width...">
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                      <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5">
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="address">
                </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4 mb-lg-0">
          <div class="package text-center bg-white">
            <span class="img-wrap"><img src="{{url('assets/images/flaticon/svg/002-target.svg')}}" alt="Image" class="img-fluid"></span>
            <h3 class="text-success">Title 2</h3>
            <div class="card-body card-block">
                <div class="form-group">
                    <input type="text" class="form-control" name="first_name" placeholder="Enter Length...">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="last_name" placeholder="Enter Width...">
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                      <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5">
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="address">
                </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4 mb-lg-0">
          <div class="package text-center bg-white">
            <span class="img-wrap"><img src="{{url('assets/images/flaticon/svg/003-mission.svg')}}" alt="Image" class="img-fluid"></span>
            <h3 class="text-danger">Title 3</h3>
            <div class="card-body card-block">
                <div class="form-group">
                    <input type="text" class="form-control" name="first_name" placeholder="Enter Length...">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="last_name" placeholder="Enter Width...">
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                      <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5">
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="address">
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

@include('base.footer')

    

    