@include('base.header')

<div class="site-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <div class="block-2 red">
            <span class="wrap-icon">
              <span class="icon-home"></span>
            </span>
            <h2>Indoor Games</h2>
            <div class="position-relative tm-thumbnail-container">
                <img src="{{url('assets/images/1.png')}}" alt="Image" class="img-fluid tm-catalog-item-img">    
                <a href="video-page.html" class="position-absolute tm-img-overlay">
                    <i class="fas fa-play tm-overlay-icon"></i>
                </a>
            </div> 
          </div>
        </div>
        <div class="col-lg-4">
          <div class="block-2 yellow">
            <span class="wrap-icon">
              <span class="icon-person"></span>
            </span>
            <h2>Outdoor Game And Event</h2>
            <div class="position-relative tm-thumbnail-container">
                <img src="{{url('assets/images/2.png')}}" alt="Image" class="img-fluid tm-catalog-item-img">    
                <a href="video-page.html" class="position-absolute tm-img-overlay">
                    <i class="fas fa-play tm-overlay-icon"></i>
                </a>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="block-2 teal">
            <span class="wrap-icon">
              <span class="icon-cog"></span>
            </span>
            <h2>Camping for Kids</h2>
            <div class="position-relative tm-thumbnail-container">
                <img src="{{url('assets/images/3.png')}}" alt="Image" class="img-fluid tm-catalog-item-img">    
                <a href="video-page.html" class="position-absolute tm-img-overlay">
                    <i class="fas fa-play tm-overlay-icon"></i>
                </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="site-section bg-info">
    <div class="container">
      <div class="row mb-5">
        <div class="col-12 text-center">
          <span class="text-cursive h5 text-red d-block">Packages You Like</span>
          <h2 class="text-white">Our Packages</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 mb-4 mb-lg-0">
          <div class="package text-center bg-white">
            <span class="img-wrap"><img src="{{url('assets/images/flaticon/svg/001-jigsaw.svg')}}" alt="Image" class="img-fluid"></span>
            <h3 class="text-teal">Indoor Games</h3>
            <p>Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p>
            <p><a href="#" class="btn btn-primary btn-custom-1 mt-4">Learn More</a></p>
          </div>
        </div>
        <div class="col-lg-4 mb-4 mb-lg-0">
          <div class="package text-center bg-white">
            <span class="img-wrap"><img src="{{url('assets/images/flaticon/svg/002-target.svg')}}" alt="Image" class="img-fluid"></span>
            <h3 class="text-success">Outdoor Game and Event</h3>
            <p>Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p>
            <p><a href="#" class="btn btn-warning btn-custom-1 mt-4">Learn More</a></p>
          </div>
        </div>
        <div class="col-lg-4 mb-4 mb-lg-0">
          <div class="package text-center bg-white">
            <span class="img-wrap"><img src="{{url('assets/images/flaticon/svg/003-mission.svg')}}" alt="Image" class="img-fluid"></span>
            <h3 class="text-danger">Camping for Kids</h3>
            <p>Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p>
            <p><a href="#" class="btn btn-success btn-custom-1 mt-4">Learn More</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

@include('base.footer')

    

    