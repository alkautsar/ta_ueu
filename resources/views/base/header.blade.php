<!doctype html>
<html lang="en">

  <head>
    <title>SupperMath Website</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700|Indie+Flower" rel="stylesheet">
    

    <link rel="stylesheet" href="{{url('assets/fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/aos.css')}}">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}">

    <link rel="stylesheet" href="{{url('assets2/fontawesome/css/all.min.css')}}"> <!-- https://fontawesome.com/ -->
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <!-- https://fonts.google.com/ -->
    <link rel="stylesheet" href="{{url('assets2/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets2/css/templatemo-video-catalog.css')}}">

  </head>

  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    
    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>



      <header class="site-navbar site-navbar-target" role="banner">

        <div class="container mb-3">
          <div class="d-flex align-items-center">
            <div class="site-logo mr-auto">
              <a href="index.html">SuperMath<span class="text-primary">.</span></a>
            </div>
          </div>
        </div>


        <div class="container">
          <div class="menu-wrap d-flex align-items-center">
            <span class="d-inline-block d-lg-none"><a href="#" class="text-black site-menu-toggle js-menu-toggle py-5"><span class="icon-menu h3 text-black"></span></a></span>

              

              <nav class="site-navigation text-left mr-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav mr-auto ">
                  <li class="active"><a href="/" class="nav-link">Home</a></li>
                  <li><a href="/material" class="nav-link">Material</a></li>
                  <li><a href="packages.html" class="nav-link">Quiz</a></li>
                </ul>
              </nav>
          </div>
        </div>

       

      </header>

    <div class="ftco-blocks-cover-1">
       
      <div class="site-section-cover overlay">
        <div class="container">
          <div class="row align-items-center ">
            <div class="col-md-5 mt-5 pt-5">
              <span class="text-cursive h5 text-red">Welcome To Our Website</span>
              <h1 class="mb-3 font-weight-bold text-teal">Bring Fun Life with SuperMath</h1>
              {{-- <p>Amazing Playground for your kids</p> --}}
              {{-- <p class="mt-5"><a href="#" class="btn btn-primary py-4 btn-custom-1">Learn More</a></p> --}}
            </div>
            <div class="col-md-6 ml-auto align-self-end">
              <img src="{{url('assets/images/kid_transparent.png')}}" alt="Image" class="img-fluid">
              
            </div>
          </div>
        </div>
      </div>
    </div>